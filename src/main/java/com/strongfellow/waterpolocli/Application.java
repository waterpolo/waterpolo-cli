
package com.strongfellow.waterpolocli;

import java.io.IOException;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.strongfellow.waterpolo.ClientBuilder;
import com.strongfellow.waterpolo.OpenClient;
import com.strongfellow.waterpolo.PropertiesFileCredentialsProvider;
import com.strongfellow.waterpolo.TradingClient;
import com.strongfellow.waterpolo.api.ActiveLoansRequest;
import com.strongfellow.waterpolo.api.ActiveLoansResponse;
import com.strongfellow.waterpolo.api.LendingHistoryRequest;
import com.strongfellow.waterpolo.api.LendingHistoryResponse;
import com.strongfellow.waterpolo.api.TickerRequest;
import com.strongfellow.waterpolo.api.TickerResponse;
import com.strongfellow.waterpolo.api.data.LoanHistory;
import com.strongfellow.waterpolo.exception.WaterPoloException;
import com.strongfellow.waterpolo.utils.Utils;

public class Application {

    @Parameters
    private static class BlankParameters {

    }

    @Parameters(separators = "=")
    private static class CommandLendingHistory {
        @Parameter(names = "--start", converter = DateTimeConverter.class)
        public DateTime start = DateTime.now(DateTimeZone.UTC).minus(Days.THREE);
        @Parameter(names = "--end", converter = DateTimeConverter.class)
        public DateTime end = DateTime.now(DateTimeZone.UTC).plus(Hours.ONE);
        @Parameter(names = "--limit")
        public Long limit = Long.MAX_VALUE;
    }


    @Parameters(separators = "=", commandDescription = "Record changes to the repository")
    private static class CommandTicker {
    }

    private static class DateTimeConverter implements IStringConverter<DateTime> {


        @Override
        public DateTime convert(String value) {
            final DateTime dt = formatter.parseDateTime(value);
            return dt;
        }
      }

    @Parameters
    private static class MainParameters {
        @Parameter(names = "--config-file")
        public String configFile = System.getProperty("user.home") + "/.waterpolo" ;

        @Parameter(names = "--format")
        public String format = "json";
    }

    private static DateTimeFormatter formatter =
            DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss").withZoneUTC();

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws IOException, WaterPoloException, InterruptedException {
        final ClientBuilder clientBuilder = new ClientBuilder();
        final OpenClient openClient = clientBuilder.getOpenClient();

        final MainParameters mainParameters = new MainParameters();
        final JCommander jc = new JCommander(mainParameters);
        jc.addCommand("ticker", new CommandTicker());

        final CommandLendingHistory lendingHistoryParameters = new CommandLendingHistory();
        jc.addCommand("lending-history", lendingHistoryParameters);
        jc.addCommand("active-loans", new BlankParameters());
        jc.parse(args);


        final String command = jc.getParsedCommand();

        final PropertiesFileCredentialsProvider provider = new PropertiesFileCredentialsProvider(mainParameters.configFile);
        final TradingClient tradingClient = clientBuilder.getTradingClient();

        switch(command) {
        case "ticker":
            final TickerResponse tickerResponse = openClient.returnTicker(new TickerRequest());
            new ObjectMapper().writerWithDefaultPrettyPrinter().writeValue(System.out, tickerResponse);
            break;
        case "lending-history":
            final LendingHistoryRequest lendingHistoryRequest = new LendingHistoryRequest();
            lendingHistoryRequest.setStart(lendingHistoryParameters.start);
            lendingHistoryRequest.setEnd(lendingHistoryParameters.end);
            lendingHistoryRequest.setLimit(lendingHistoryParameters.limit);
            final LendingHistoryResponse lendingHistoryResponse = tradingClient.returnLendingHistory(lendingHistoryRequest, provider.getCredentials());
            if ("json".equals(mainParameters.format)) {
                Utils.getObjectMapper().writeValue(System.out, lendingHistoryResponse);
            } else {
                Application.printText(lendingHistoryResponse);
            }
            break;
        case "active-loans":
            final ActiveLoansRequest activeLoansRequest = new ActiveLoansRequest();
            final ActiveLoansResponse activeLoansResponse = tradingClient.returnActiveLoans(activeLoansRequest, provider.getCredentials());

            if ("json".equals(mainParameters.format)) {
                Utils.getObjectMapper().writeValue(System.out, activeLoansResponse);
            } else {
                Application.printText(activeLoansResponse);
            }
            break;
        default:
            throw new RuntimeException("command not recognized: " + command);
        }
    }

    private static void printText(ActiveLoansResponse response) {
        throw new RuntimeException();
    }

    private static void printText(LendingHistoryResponse lhr) {
        // {"id":281144441,"currency":"BITCOIN","rate":"0.00014399","amount":"0.00045808","duration":"0.00040000","interest":"1E-8","fee":"0E-8","earned":"1E-8","open":"2017-03-13 13:57:13","close":"2017-03-13 13:57:44"}
        for (final LoanHistory lh : lhr.getHistory()) {
            System.out.format("%d\t%s\t%.10f\t%.10f\t%.10f\t%.10f\t%.10f\t%.10f\t%s\t%s\n",
                    lh.getId(), lh.getCurrency(),
                    lh.getRate(),
                    lh.getAmount(),
                    lh.getDuration(),
                    lh.getInterest(),
                    lh.getFee(),
                    lh.getEarned(),
                    lh.getOpen().toString(formatter),
                    lh.getClose().toString(formatter));
        }
        System.out.flush();
    }

}
